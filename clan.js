var clan = {
    'timeout': 10000,
    'concurrency': 15,

    'is_lan_ip': function(ip) {
        var ip_parts = ip.split( '.' );
        if(ip_parts.length !== 4) {
            return false;
        }
        ip_parts = ip_parts.map(Number);
        console.log(ip_parts);
        if (ip_parts[0] == 10 || (ip_parts[0] == 192 && ip_parts[1] == 168) ||
            (ip_parts[0] == 172 && ip_parts[1] >= 16 && ip_parts[1] <= 31)) {
            return true;
        }
        return false;
    },

    'find': function(url, found_cb, done_cb) {
        var waiting = [], running = [], total = 0;
        function done() {
            running.pop();
            manage_queue();
        }
        function manage_queue() {
            if (waiting.length > 0 && running.length < clan.concurrency) {
                var ip = waiting.pop();
                running.push(ip);
                clan.check_resource_exists(url, ip, found_cb, done);
            }
            if (waiting.length == 0 && running.length == 0) {
                done_cb(total);
            }
        }
        function process_address(ip) {
            if (clan.is_lan_ip(ip)) {
                var ip_parts = ip.split( '.' );
                for(var i = 2; i < 255; i++) {
                    var tmp_ip = ip_parts[0] + '.' + ip_parts[1] + '.' + ip_parts[2] + '.' + i;
                    waiting.push(tmp_ip);
                    total++;
                    manage_queue();
                }
            }
        }
        clan.interface_ips(process_address);
    },

    'router': function(found_cb) {
        function process_address(ip) {
            if (clan.is_lan_ip(ip)) {
                var ip_parts = ip.split( '.' );
                var rtr_ip = ip_parts[0] + '.' + ip_parts[1] + '.' + ip_parts[2] + '.1';
                found_cb(rtr_ip);
            }
        }
        clan.interface_ips(process_address);
    },

    'interface_ips': function(found_cb) {
        var RTCPeerConnection = window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
        if (!RTCPeerConnection) {
            console.log("WebRTC/RTCPeerConnection not supported");
        }
        var addrs = Object.create(null);
        addrs['0.0.0.0'] = false;
        function addAddress(newAddr) {
            if (newAddr in addrs) return;
            addrs[newAddr] = true;
            found_cb(newAddr);
        }
        function grepSDP(sdp) {
            var hosts = [];
            sdp.split('\r\n').forEach(function (line) {
                if (~line.indexOf('a=candidate')) {
                    var parts = line.split(' '),
                        addr = parts[4],
                        type = parts[7];
                    if (type === 'host') 
                        addAddress(addr);
                } else if (~line.indexOf('c=')) {
                    var parts = line.split(' '),
                        addr = parts[2];
                    addAddress(addr);
                }
            });
        }
        var rtc = new RTCPeerConnection({iceServers:[]});
        rtc.createDataChannel('', {reliable:false});
        rtc.onicecandidate = function (evt) {
            if (evt.candidate) 
                grepSDP('a='+evt.candidate.candidate);
        };
        rtc.createOffer(function (offerDesc) {
            grepSDP(offerDesc.sdp);
            rtc.setLocalDescription(offerDesc);
        }, function (e) {});
    },

    'check_resource_exists': function( resource, ip, success_cb, done_cb ) {
        var full_source = 'http://' + ip + ( resource instanceof Array ? resource[0] : resource );
        var xhr = new XMLHttpRequest();
        xhr.open('GET', full_source);
        xhr.timeout = clan.timeout;
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    success_cb(ip, xhr.responseType, xhr.response);
                }
                done_cb(ip);
            }
        };
        xhr.send();
    }
}
