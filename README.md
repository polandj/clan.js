# clan.js
A small library for working with a client browser's LAN.  Works via WebRTC IP enumeration and ajax host scanning.

## What's this for?
To help your users find your device on their LAN.

## How do I use it?
Suppose you want to locate a device.  You can identify the deviceby a particular URL and maybe it also responds to mDNS. 
By hosting a file in a well-known location (e.g. in the cloud), you can help your users find the device in their LAN by 
going to the well-known location.  Take a look at the example.html for an example.

## How does it work
It uses WebRTC to find your local LAN IP.  From there, it assumes you're on a /24 network and will issue AJAX requests to
the other hosts on your LAN looking for the well known URL.